---
title: Welcome
description: Hello
tags:
    - General
---

## SWM Wiki ##
Welcome to the SWM (unofficial) wiki

## Facebook communities ##

[UK owners club](https://www.facebook.com/groups/617674261721592){target=_blank rel=noopener}
[GB Owners club](https://www.facebook.com/groups/590543751569200/){target=_blank rel=noopener}

[Australia & New Zealand owners](https://www.facebook.com/groups/1913195662226305){target=_blank rel=noopener}

[USA Riders](https://www.facebook.com/groups/2147511958614522){target=_blank rel=noopener}

[Husqvarna SM 450/510R](https://www.facebook.com/groups/218709664984269){target=_blank rel=noopener}


## About me ##
I'm SM26 and I own a 2019 SWM SM500R (EU version)
I have a [YouTube channel](https://www.youtube.com/SM26 "SM26") and an [Instagram account](https://www.instagram.com/sm26.official/ "Instagram") that you are more than welcome to check out

## How to use this Wiki ##
Links to outside sources will open up in a new window
you can tell if a link is to some other site if it's looks like this ![outside link](https://sm26.gitlab.io/swm_wiki/assets/images/outsidelink.png){.tiny}{ .off-glb }

Abbreviations will have dotted line under them, on PC you can hover with your mouse over the word to see what it mean, on mobile it will jst show you the meaning in brackets.

On PC you can hit the ++s++ or ++f++ key to open up the search bar

Click on a picture to see it in full screen

Click on the SWM logo to return to the main page.

Please note that all of the eBay links in here are affiliate links.

If you can't see images properly, most likely you don't have WebP codec.
For older PCs (Like Windows XP) the following [installer by Google](https://storage.googleapis.com/downloads.webmproject.org/releases/webp/WebpCodecSetup.exe){target=_blank rel=noopener} needs to be used

### SWM Wiki as an app ###

=== "Windows (PWA)"
    Windows support PWA, which means that you can add a desktop shortcut and the site will behave like an app(!)
    To do so, here is an example of how to do it in Chrome
    (It works on other browsers, with minor differences)

    1. While on the main page (this one), click on the 3 vertical dots at the top right corner.
    2. Click on "more tools"
    3. Then "create shortcut..."
    ![!PWA1](https://sm26.gitlab.io/swm_wiki/assets/images/PWA1.webp){.medium}
    4. In the pop up box, be sure to check the "open as window" box
    ![!PWA2](https://sm26.gitlab.io/swm_wiki/assets/images/PWA2.webp){.medium}
    5. Profit!

    Now you should see a shortcut on your desktop, and you can view it as a separate app and not as a Chrome tab.

=== "Android app"
    On Android it's even simpler!

    1. While on the main page (this one), click on the 3 vertical dots at the top right corner.
    2. Click on "add to home screen"
    3. Click "add"
    4. Click "add automatically"
    5. Profit!

    Now you should see a shortcut on your phone as an SWM Wiki app.
    Only downside, you don't see the URL anymore, so it will be a bit hard to share links and stuff....

{!docs/todo.txt!}



