---
title: Contribute
description: How to contribute
tags:
- General
disqus:
---

# Contribute #

## How to make a document ##

### Metadata ###
start each page with:
``` ymal
---
title: Name Of The Page
description: Description
tags:
    - topic
---
```
you can also add ``` disquas:``` to turn off the comments section for a specific page

### Abbreviations ###
add ```{ ! docs/abbreviations.txt!}``` (without spaces near the "!") at the end of each page to use all of the available abbreviations.

### Links ###
wrap each link like the following: ``` [Link text](https://www.website.com){target=_blank rel=noopener} ```
add ``` {target=_blank rel=noopener} ``` at the end of each link to make it open in a new tab

### Images ###
To add a picture, use the following syntax ``` ![!Short Description](../assets/images/picName.jpg){.center .large} ```
You can add ```{.center} ``` tag to make to picture centerly alligned insted of LTR
you can add sizes tage as well:
```{.tiny} ``` (35px)
```{.xsmall} ``` (80px)
```{.small} ``` (150px)
```{.medium} ``` (210px)
```{.large} ``` (300px)
```{.xlarge} ``` (450px)
```{.big} ``` (600px)

## Cheat sheets ##

[Markdown Guide](https://www.markdownguide.org/cheat-sheet/){target=_blank rel=noopener}
[Github Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet){target=_blank rel=noopener}
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/specimen/){target=_blank rel=noopener}
[Yakworks cheat sheet for MkDocs Material](https://yakworks.github.io/mkdocs-material-components/cheat-sheet/){target=_blank rel=noopener}
[3OS Markdown CheatSheet For MkDocs](https://3os.org/markdownCheatSheet/welcome/){target=_blank rel=noopener}

## How to reach me ##

You can reach me at my [E-mail](mailto:Gaist.Or+SWM@gmail.com)
Or use one of the links at the bottom of the page to reach me on Telegram
