---
title: DAB Motors
tags:
    - General
---
# DAB Motors #

## LM-S ##

Some nice articels about it:<br>
[BikeExif](http://www.bikeexif.com/euro4-husqvarna-510-custom){target=_blank rel=noopener} <br>
[New Atlas](https://newatlas.com/dab-lm-s-custom-supermoto/59897/){target=_blank rel=noopener}<br>
[Canada Moto Guide](https://canadamotoguide.com/2019/05/30/dab-offers-euro-buyers-a-truly-unique-custom-buying-experience/){target=_blank rel=noopener}

And I've saved the best for last,
One of the designers shared his inside knowledge and took us through the steps, really interesting IMO:
[Blender Artists](https://blenderartists.org/t/dab-motors-lm-s/1162275){target=_blank rel=noopener}
{!docs/abbreviations.txt!}