---
title: SWM Prototypes
tags:
    - General
---
## RS 340 S
The RS340S was first seen at EICMA 2015 Milano.
was thought to be released at 2017, but never made it into production

[!embed](https://www.youtube.com/watch?v=fzlNUL72oc8)

![!RS340S](../assets/images/RS340S/RS340S2.webp) {.large}
![!RS340S](../assets/images/RS340S/RS340S1.webp) {.large}
![!RS340S](../assets/images/RS340S/RS340S3.webp) {.large}
![!RS340S](../assets/images/RS340S/RS340S4.webp) {.large}
![!RS340S](../assets/images/RS340S/RS340SEngine.webp) {.large}

## MC 250 S
The MC250S was first seen at EICMA 2015 Milano.
Was thought to be released at 2017, but never made it into production
Based on this engine AJP biuled thier SPR 250, made to be some sort of MX
![!MC250S](../assets/images/MC250S/MC250S.webp)

## RZ4.21
This was only a concept, only shown at EICMA 2016 Milano.

## RS250R & SM250R ##
I know someone is thinking RS250??? what is that? In Malaysia, bikes larger than 250cc are hard to get approved, so the Malaysian importer special ordered enough bikes in 250cc size that a special run was produced for them. [^soruce]

[!embed](https://www.youtube.com/watch?v=tPJytihOW3U)

[^soruce]: [SWM USA FB Page](https://www.facebook.com/permalink.php?story_fbid=2211723825802757&id=1509674789341001 "SWM USA FB Page"){target=_blank rel=noopener}
