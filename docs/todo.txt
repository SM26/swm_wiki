## To-Do list ##

* [x] build the skeleton (template) for the wiki
    * [x] make it Progressive Web Apps (PWA) compatible
    * [x] make it Android compatible 
    * [x] make lists look better in dark mode 
    * [x] make footnotes look better in dark mode
    * [x] make external links open up in a new window
    * [x] make RELATIVE image implementation much easier
    * [x] embed PDFs properly
    * [x] make images sizeable and zoomable
    * [x] make abbreviations work on mobile as well
    * [x] make a custom 404 page
* [x] SEO - Robots, sitemap and google index & Google Analytics (hopefully)
* [X] Files size optimization: Files to be compressed by GZ and images converted to WEBP
* [ ] add info about the RS models
* [ ] add info about the SuperDual models
* [ ] add info about the Six Days models
* [ ] add a spell checker
* [ ] lightbox2 gallery
* [X] add tags to each post


