---
title: Pre Delivery Inspection
tags:
    - SM500R
---

# Pre delivery inspection #
When you buy any bike, you should do a series of pre delivery inspections

## Exhaust header ##
Make sure that the chain slider isn't touching the exhaust header
![!SliderHeader](../assets/images/SliderHeader.webp "chain slider is touching the header"){.medium}

## ABS cable ##
Make sure that the cable is routed on the inside, you should __NOT__ see the metal tabs.
![!ABS Cable](../assets/images/ABSline.webp "This is the wrong way"){.medium}

## Fender rubber grommets ##
Make sure the two rubber grommets on each side of the exhaust are there, otherwise it will melt your fenders.
