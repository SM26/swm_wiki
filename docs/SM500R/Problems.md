---
title: Known problems
tags:
    - SM500R
---

# Common problems and issues #
In here I'll try to cover every problem I had
If you have a solution for it, __please let me know!__

* Speedo shows km/h speed even in stand still.
* In cold weather (below 10^C^) the low fuel warning light will not work.
* Exhaust header bolt comes off
* Make sure you tighten the ignition key, or it will come off! ![!Ignition Key](../assets/images/LostKey.webp){.small}
* The exhaust braket on the right side is prone to cracking. keep an eye on it.[^1] ![!CrackedExhaustBraket.webp](../assets/images/CrackedExhaustBraket.webp){.small}
* The side stand bolt is prone to backing out so you should add loctite. [^2]

[^1]:[Facebook](https://www.facebook.com/groups/218709664984269/permalink/1404639586391265){target=_blank rel=noopener}
[^2]: Thanks for Goki for pointing this common issue out.
{!docs/abbreviations.txt!}
