---
title: How to adjust cam timing
tags:
    - SM500R
---


# How to adjust cam timing

## Valve Shims
Our SWM SM\RS uses 9.48mm diameter Shims.
Old Husky uses 7.48 , Don't use them!

## Credit and disclaimer
I didn't write any of this, all credit goes to [dfeckel](http://www.cafehusky.com/members/dfeckel.145/){target=_blank rel=noopener} from [CafeHusky](http://www.cafehusky.com/threads/foolproof-cam-timing.3108/){target=_blank rel=noopener} <br>
All of this was for the 2009 Husqvarna, so it should be the same for SWM SMR, but any verification would be much appriciated.

## Intro
In order to ensure that the cam timing is correct you don't need to mark anything before pulling it all apart, all of the marks are there for you already

There are marks on the cam sprocket, and the two marks on the gear portion of the cam sprocket align with the line on the head to indicate TDC.
Here is the tricky bit I figured out; When at TDC, the OTHER marks on the gear portion of the cam sprocket align with the marks on the cam gears!! The thing is, you can't SEE them aligning at TDC--your vision is obstructed by the head casting. Here's how to use those marks.


## Step 1
With the cams out, get the cam sprocket and timing chain reinstalled with the front chain slider and tensioner installed. You probably don't need the tensioner installed for this, but I wanted the maximum accuracy when verifying TDC. With the spark plug out, put something with a long, smooth, thin handle (like an hex wrench) into the spark plug hole so it is resting on the top of the piston. This is your TDC indicator.
![!Verify TDC](../assets/images/CamTiming/VerifyTDC.webp){.xlarge}
Carefully move the engine through a few revolutions to verify the TWO marks align with the mark on the head when the hex wrench stops moving upwards. When you are satisfied that the cam sprocket and timing chain are properly installed, move on to step 2.

## Step 2
Use the kickstarter to carefully inch the engine to just before TDC.
![!Just Before TDC](../assets/images/CamTiming/JustBeforeTDC.webp){.xlarge}
When you do this, you will see another mark on the gear portion of the cam sprocket come into view. If the TWO marks are at the 12 o'clock postion, then this mark is at about 2 o'clock. You couldn't see it before since it was obscured by the head casting.

## Step 3

Grab the exhaust cam and find the mark on the gear.
![!Cam Mark](../assets/images/CamTiming/CamMark.webp){.xlarge}
While holding the exhaust cam in your hands, mesh the teeth together with the cam sprocket at the marks.
![!Exhaust Cam Lined Up 1](../assets/images/CamTiming/ExhaustCamLinedUp1.webp){.xlarge}
The tooth on one is marked to mesh with the valley on the other.
![!Exhaust Cam Lined Up 2](../assets/images/CamTiming/ExhaustCamLinedUp2.webp){.xlarge}
Now, you can't FULLY mesh the gears at the marks because the cam isn't aligned with its recesses in the head. But, with your hands tilting the cam up a little, you can mesh the edges of the cam teeth. (confused, yet? Good thing I don't write instruction manuals!) When you're sure the marked tooth meshes with the marked valley, "walk" the cam down to its recesses in the head without unmeshing the gears.
![!Exhaust Cam Walking](../assets/images/CamTiming/ExhaustCamWalking.webp){.xlarge}
You're rolling the cam gear around the gear portion of the cam sprocket until the cam rests in its proper place in the head.
![!Exhaust Cam Positioned](../assets/images/CamTiming/ExhaustCamPositioned.webp){.xlarge}
## Step 4
Snug down the exhaust cam retaining caps.
![!Exhaust Cam Retainer Caps](../assets/images/CamTiming/ExhaustCamRetainerCaps.webp){.xlarge}
## Step 5
Carefully turn the engine over ~~with the kickstarter~~ a few times. There should be no interference.

## Step 6
Turn the engine over to just AFTER TDC. You should still see the mark on the gear portion of the cam sprocket that aligns with the mark on the intake cam. This mark is at about the 10 o'clock position relative to the two marks

## Step 7
Repeat [step 3](https://sm26.gitlab.io/swm_wiki/SM500R/CamTiming/#step-3) with the intake cam.
![!Intake Cam Lined Up](../assets/images/CamTiming/IntakeCamLinedUp.webp){.xlarge}
![!Intake Cam Walking](../assets/images/CamTiming/IntakeCamWalking.webp){.xlarge}
![!Intake Cam Positioned](../assets/images/CamTiming/IntakeCamPositioned.webp){.xlarge}
Snug down the caps.
![!Retainer Caps On](../assets/images/CamTiming/RetainerCapsOn.webp){.xlarge}

## Step 8
Turn the engine over a few times and put it back to TDC.

## Step 9
Remove the left side cam caps so you can reinstall the oil line.
![!Oil Line](../assets/images/CamTiming/OilLine.webp){.xlarge}
## Step 10
Torque the cam caps
![!Complete](../assets/images/CamTiming/Complete.webp){.xlarge}
{!docs/abbreviations.txt!}


## YouTube Video toturials
use as refrence, they are for the old Husky, but most of it apply to us as well

<iframe width="560" height="315" src="https://www.youtube.com/embed/uf0AKO-_tI4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/jSfmRTWoGjk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
