---
title: Cross referencing parts
tags:
    - SM500R
---

# Cross referencing parts #

## Front sprocket ##
Stock SM500R is 15 teeth
Aprilia MXV/RXV/SXV 450-550 will fit right on the SM500R
AFAM P/N: 37400-15
JT Sprocket P/N: JTF707
![!JTF707](../assets/images/JTF707.webp){.small}

## Rear sprocket ##
Stock SM500R is 42 teeth

JT Sprocket P/N: JTR822.42 (Steel)
JT Sprocket P/N: JTA822.45 (Aluminum)

??? info "Beta Motor compatible"
    350RR 11-12
    400RR 05-12
    450RR 05-09
    525RR 05-09

??? info "Gas Gas compatible"
    EC 125 Racing 2013
    EC 250 E 2013
    EC 250 F (4T) 2012
    EC 300 E 2013-2014
    EC 300 F Six Days (4T) 2012

??? info "Husqvarna compatible"
    CR 125 95-12
    WR(E) 125 95-13
    TC(R) 250 02-13
    TE 250 02-13
    TXC 250 08-12
    TE 310 09-12
    TE 449 11-12
    SMR 449 2011
    TC 450 02-10
    TE 450 02-06
    TC 510 06
    TE 510 05-10
    TXC 510 09-11
    SM 510 R 05-10
    TE 511 11-12
    SM 530 RR 2009
    TE 630 10-12
![!JTA822](../assets/images/JTA822.webp){.small}

## Oil filter ##
Aprilia RXV/SXV 450-550 2009-2015 P/N: 9150166
Old Husqvarna P/N: 8000B0593
HiFlo HF563 is a direct replacement

## Clutch ##
MV Agusta may be using the same clutch, but with different part numbers.
The SWM P/N is for the whole kit, while at MV it separated

So, any confirmation would be much appreciated.

??? question "MV Agusta"
    Friction plates 8000A6916 - 8 needed
    Metal plates 8000A6918 - 9 needed

    Please note that MV Agusta uses different friction plate depending on its location
    Outer friction plates 8000A6917 - 2 needed
    One before the most outer friction plate 8000A6915
    They also use 2 types of clutch support springs, which I don't think we need
    Clutch plate spring 8000A6922
    Clutch support spring 8000A6923

## Frame Guards ##
Older models had plastic frame covers, but the new Euro4 has a Sticker type instead
![!FrameGuards](../assets/images/FrameGuards.webp){.medium}
But if you want, you can still put on the "old" ones, you do need to trim it a bit, and it's not a 100% perfect fit
But it will fit!
And yes, Old Husqvarna WR/CR 125 should be just the same.

LH side P/N: 8000H3435
RH side P/N: 8000H3434

## Timing chain ##
MV Agusta uses the same timing chain
P/N: 8B0035794
96 links

