---
title: ECU
tags:
    - General
---

# ECU tuning and flashing #
Our bike use a fuel injection system by Athena
In order to change map you need their software called Maya[^1]

This method is not excusive to the SM500R and should be the same for the following models as well
??? info "SWM"
    SWM RS500R
    SWM RS300R 
    SWM RS125R 
    SWM SM500R 
    SWM SM125R 
    SWM GRAN MILANO 
    SWM GRAN TURISMO 
    SWM SILVER VASE 
    SWM SUPERDUAL 
    SWM RS650 R
    SWM RZ4.21

I managed to get my hands on an instructions PDF for the German dealers, it goes into great detail, but it's in German
And the username and password for the maps download don't work anymore

[German diagnostic and new maps update](../assets/PDFs/RS_02_16_Diagnosetool_Update_und_neue_Maps_Version_1.pdf "German diagnostic and new maps update")
<embed src="../../assets/PDFs/RS_02_16_Diagnosetool_Update_und_neue_Maps_Version_1.pdf" type="application/pdf" width="100%" height="500">

## Maya ##
You can download Maya (v0.10.8) in this [link](../assets/Maya_Install_0.10.8.exe).
[Backup link of 10.5](http://www.getdata.it/data/download/Power/Maya_Install_0.10.5.exe){target=_blank rel=noopener}
It's not the full version, to get all of it's powers we need an OEM license.

Athena is using some proprietary/special file extension for the Maya software.
MYA are device file. theye are used to read the ECU and serve as basic layout.
MYP are "map" files. It's basically an archive with some metadata.
MYM map file. seems to be some kind of binary file.
MPJ project file. useful if you want to backup what you've got so far.
MLG is log file.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Yj68wOonIfQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Maya Software User Manual - ver v0.10.8
A very extensive [PDF](http://axendp.com/img/cms/MU_Maya_rev41_ENG.pdf){target=_blank rel=noopener} about how to use it.

[SWM Maya software Help Guide](../assets/PDFs/SWM - Maya_Helpguide_ENG.pdf "Maya Help Guide")
<embed src="../../assets/PDFs/SWM - Maya_Helpguide_ENG.pdf" type="application/pdf" width="100%" height="500">

## Diagnostic cable ##
To connect to the ECU, you can buy the cable, SWM P/N: [E000P00925](https://www.motosati.pl/swm/6122-ecu-programming-wire.html){target=_blank rel=noopener}

Other (untested) option is aftermarket cable by Texa model: [3151/AP51 SWM 3907938](https://www.carmo.nl/index.php?main_page=product_info&products_id=2528){target=_blank rel=noopener}

### Make your own cable ###
You can make it own your own using this diagram:
![!DIY Cable](../assets/images/MayaCable.webp){ .large}![!DIY Cable3](../assets/images/MayaCable3.webp){ .large}
![!DIY Cable2](../assets/images/MayaCable2.webp){ .large}


??? info "Part list:"
    Econoseal J Series 6 Pin Male Connector [eBay Link](http://rover.ebay.com/rover/1/711-53200-19255-0/1?ff3=4&pub=5575597703&toolid=10001&campid=5338700543&customid=&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F274124007485){target=_blank rel=noopener}/[AliExpress](https://a.aliexpress.com/_vl3zEX){target=_blank rel=noopener}
    [FTDI USB-RS232-WE-1800-BT-5.0 CABLE](https://a.aliexpress.com/_vmnKTd){target=_blank rel=noopener}
    [2 Pin jumper connector](https://a.aliexpress.com/_vSXYMP){target=_blank rel=noopener}

## Drivers ##
After we have a Cable, we need the appropriate drivers.
For me, [Virtual COM Port Drivers](https://www.ftdichip.com/Drivers/VCP.htm){target=_blank rel=noopener} did the trick, but I've also installed [D2XX Direct Drivers](https://www.ftdichip.com/Drivers/D2XX.htm){target=_blank rel=noopener} just to be on the safe side.
I've personally use the executable type for convenience.

## TPS ##
It's always a good idea to make sure that your TPS is properly set up
<iframe width="560" height="315" src="https://www.youtube.com/embed/xPQTNPXHn4k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Maps ##
Device file for the SM500R: [MB2SWM_M51C20F53_r02_GET15.mya](https://gitlab.com/SM26/swm_wiki/-/raw/SM500R-Maps/docs/assets/Maya/500/MB2SWM_M51C20F53_r02_GET15.mya?inline=false){target=_blank rel=noopener}.
Device file for the RS300R: [MB2SWM_M51C20F52_r03_GET15.mya](https://gitlab.com/SM26/swm_wiki/-/raw/SM500R-Maps/docs/assets/Maya/300/RS%20Full%20power%20Euro4/MB2SWM_M51C20F52_r03_GET15%20(2).mya?inline=false){target=_blank rel=noopener}.
Device file for the RS125R: [MB2SWM_M51C20F53_r01_GET15.mya](https://gitlab.com/SM26/swm_wiki/-/raw/SM500R-Maps/docs/assets/Maya/125/MB2SWM_M51C20F53_r01_GET15.mya?inline=false){target=_blank rel=noopener}.


Want to backup you own map?
here is a short video on how to do it:
<iframe width="560" height="315" src="https://www.youtube.com/embed/TXMnkAAxVLY" title="SM26 ECU Backup" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Map switcher ##
Only works with Euro3 models, will not work for Euro4
More info (in German) [here](https://www.ebay.com/itm/164573546683){target=_blank rel=noopener}

[^1]: I could not make TuneECU to work with SWM, so it's not an option to replace Maya

{!docs/abbreviations.txt!}
