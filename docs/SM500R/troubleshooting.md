---
title: Troubleshooting
tags:
    - SM500R
---

# Troubleshooting guide #
Something isn’t working properly?
Got a problem and don't know what to do now?

You've come to the right place

## Trip 1 isn't counting! ##

If the STP 1 is set to zero, the functions TRIP 1 and AVS1 will also be set to zero. The function TRIP 1 is ON together with the function STP 1.
So to put it simply, cycle through the speedo options until you see STP
Long press the button for 4 seconds, and the timer will start to count time
Long press again for 4 seconds to cycle to the next function, and then short press until the speedo shows you what you want to know
Trip 1 will work once more.

## Gas cap is really hard to open! ##

Make sure that the rubber gasket that is inside the cap is seated properly.
Also, using the original gas cover doesn't hurt
