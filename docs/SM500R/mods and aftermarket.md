---
title: Aftermarket add-ons
tags:
    - SM500R
---

# Aftermarket add-ons and Modifications #

## Clutch ##

### Clutch master ###

The obvious choice will be Magura 163
But while you are at it, why not just fit a Magura 167?

??? info "Magura"
    Master cylinder model 167 9.5 piston diameter for mineral fluid left side P/N:2000302
    Hot Start/Decomp Lever P/N: 0723189
    Mirror clamp black with m10x125 threads P/N: 2700177
    Safety start switch P/N: 0723187 

    Magura 167 is also known under the name Hymec    

Personally, I went with Clake One Light Clutch

### Clutch lever ###
Because we don't have an original Magura
The following **WILL NOT FIT**

??? warning "Don't buy this"
    Magura short lever P/N: 0720600
    KTM (Magura) lever P/N: 59002031000
    RFX P/N: fxcl 50100 005V

??? info "Options that should fit"
    KTM 640/690 (2003-2013) P/N:54602031000
    AS3[^1] UPC: [0682698338292](https://www.as3performance.co.uk/swm-rs-300-500-r-2016-2019-front-brake-and-clutch-levers-set-black/){target=_blank rel=noopener}
    AS3[^1] "Flexi" UPC: [0682698335888](https://www.as3performance.co.uk/swm-rs-300-r-rs-500-r-2016-2019-front-brake-clutch-flexi-levers-black/){target=_blank rel=noopener}

Stock vs RFX
![!Stock vs RFX](../assets/images/RFXlever.webp){.medium}

### Clutch slave ###
Oberon makes a perfect replacement - [CLU-1500](http://www.oberon-performance.co.uk/cgi-bin/sh000001.pl?WD=8000%2099928&PN=Husqvarna%2ehtml#SID=5711?aCLU_2d1500){target=_blank rel=noopener}

### Clutch cover ###
There are mainly two types of covers:
Bolt on - this will go on top of your stock case
Replacement cover - stronger material with added oil capacity

??? Info "Bolt on cover"
    EnduroHog make clutch and ignition bolt on covers
    You can buy it [at this link](http://rover.ebay.com/rover/1/711-53200-19255-0/1?ff3=4&pub=5575597703&toolid=10001&campid=5338700543&customid=&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F323895923603){target=_blank rel=noopener}

??? Info "Replacement cover"
    Rekluse P/N: RMS-356
    AS3 UPC: [0644824129457](http://rover.ebay.com/rover/1/711-53200-19255-0/1?ff3=4&pub=5575597703&toolid=10001&campid=5338700543&customid=&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F163977607084){target=_blank rel=noopener}

## Front Disc / Rotor ###
![!Front rotor](../assets/images/FrontDiscSM500R.webp){.medium}
OEM is Galfer DF645CW-ALU.
SWM P/N -  F000P01803
But Galfer isn't the only option:
Braking - WK059L
Moto Master - 113039
EBC - MD6293D
And maybe even Brembo 108.A642.16

## Case Saver ##
Moose racing makes a case saver for the Husqvarna 2005-2010 TC/TXC/TE 450-510
P/N: [0950-0380](https://www.ebay.com/itm/264657459662){target=_blank rel=noopener}

## Charcoal canister removal ##
The charcoal canister is there to meet the Euro4 emissions standards
But you can remove it with no repercussions
![!Charcoal canister](../assets/images/Canister.webp){.xsmall}
Just take it off, cover the electrical connector and plug some screw in the engine M5X0.8X8 (ZD0067997)
![!Engine bolt locatoin](../assets/images/Canister2.webp){.xsmall}
Alternatively you can use a vacuum nipple
![!Engine bolt locatoin](../assets/images/Canister3.webp){.xsmall}

## Steering damper ##
[GPR has a kit for SWM](https://www.gprstabilizer.com/find-kit/?type=dirt-bikes&make=swm&model=rs500r&years=2019){target=_blank rel=noopener}
![!GPR Steering Stabilizer on RS500R](../assets/images/gprSteeringStabilizer.webp){.small}
credit goes to James Burlis for making sure it is a good fit.
please note that you may need to trim the speedo mount plastic on the left and right to clear the bar clamp.

For whatever reason SWM changed the triple tree and the risers bolt centers, so Husqvarna  TE kit will not fit.
Is has been noted that a Beta sub mount should fit (with some small modifications)[^2]
Scott's kit for the 2010 TE 510 __WILL NOT FIT__ ~~[P/N: DS-SUB-6030-01](https://www.scottsonline.com/Stabilizer_Purchase2.php?Bike_ID=5371&BI_ID=138431){target=_blank rel=noopener}~~ [^2]
~~Husqvarna Power Parts [P/N: 8000H7798](https://www.ktm-parts.com/8000H7798.html){target=_blank rel=noopener}~~

Big credit to [Steve McLean](https://www.facebook.com/steve.mclean.14)[^2] who found a way to make the Scott kit work:
* Husqvarna Frame Bracket and post (2009 TE501).
* Bar clamp for 2014 Beta 520RS (1 1/8" bars, stock triple clamp, furthest from rider).
* Scott's LKM-4032-48 (+8mm) link arm stepped.
* Husqvarna 2004 TE450 Bar Mounts (they will fit the Beta bar clamp).
* Scott's stabilizer.
![!Scott Steering Stabilizer on RS500R](../assets/images/ScottSteeringDamper.webp){.small}


[Ohlins SD2.1](http://www.hardracing.com/ohlinssd20.htm){target=_blank rel=noopener} is looking promising, but because of its price, I haven't tested it.
Another option is [FastWay](https://fastway.zone/product-category/all/steering-stabilizers/){target=_blank rel=noopener}, but they don't list SWM, so it's still vague if it will fit.


## O2 removal ##
In order to plug the hole that's left in the header when removing the O2 sensor, you will need a plug that is 12x1.25 mm.
In order to deal with the electronics you also need a resistor with value of 2.2k ohm.
It has been noted that SMC690 07-17 o2 eleminator kit will fit the SM500R.
[eBay link](https://www.ebay.com/itm/322355638591){target=_blank rel=noopener}

## Side stand eliminator ##
In order to remove the hall sensor without triggering an error code, we need to put a 1K Ohms resistor at pins 1 <--> 3
Color code: Black|Brown|Orange|Gold
The connector is a generic 3 pin waterproof connector
![!Hall Sensor Delete](../assets/images/HallSensorDelete.webp){.small}

## Starter relay ##
The stock one is prone the early fail, so while you’re at it, just put a better one:
[Starter Relay for Husqvarna TE 310 (4T) (Europe) 2009-2012](https://awmotorcycleparts.co.uk/starter-relay-yamaha-fz-6-xvs650-gts1000-xv1700-ybr250-yfms){target=_blank rel=noopener} 
Use 10% coupon code: AW1
Or if you prefer an [eBay link](http://rover.ebay.com/rover/1/711-53200-19255-0/1?ff3=4&pub=5575597703&toolid=10001&campid=5338700543&customid=&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F401497520205){target=_blank rel=noopener} 
**please note that on this one the terminals are swapped!**

![!Starter Relay](../assets/images/StarterRelay.webp){.small}

## Starter ##
The stock starter is prone to an early death in hot conditions,
The following are compatible replacements to stock one P/N:8000B1373

H222AB7V0 / HA222AB8V0 / HA222AB9V / HA224AB9V0
or a [Ducati stater](http://rover.ebay.com/rover/1/711-53200-19255-0/1?ff3=4&pub=5575597703&toolid=10001&campid=5338700543&customid=&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F283697808236){target=_blank rel=noopener} P/N: 0530 3408
![!DucatiStarter](../assets/images/DucatiStarter.webp){.small}

## Swing arm cover ##
If you want to add some protection to you swing arm from scratches, for 38 Euros you could buy from [RinghioRacing.it](https://www.ringhioracing.it/it/home/42-protezione-forcellone-rs300-500-swingarm-cover.html){target=_blank rel=noopener}

## Oil filter ##
HiFlo HF563 is a direct replacement.

## 3D Printed parts ##
I've modeled and printed a few parts for the SM500R, but it may fit other models as well.
Stem nut cover - [STL](../assets/STLs/SMStemCoinFinal.stl)

<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/CHSKAC7lm8G/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="13" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/CHSKAC7lm8G/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/CHSKAC7lm8G/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by @sm26.official</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2020-11-07T08:59:31+00:00">Nov 7, 2020 at 12:59am PST</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

Seat bolt washers - [3MF](../assets/STLs/SWM Seat washer.3mf) / [STL](../assets/STLs/SWM Seat washer.stl)
[!embed](https://www.youtube.com/watch?v=v9tmkyWgp30)

## Front Forks Mud Scraper ##
SKF Fork Mud Scraper P/N:[KIT-MS48KM](http://rover.ebay.com/rover/1/711-53200-19255-0/1?ff3=4&pub=5575597703&toolid=10001&campid=5338700543&customid=&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F401245232762){target=_blank rel=noopener} should fit right in.
Credit goes to [James Burlis](https://www.facebook.com/photo/?fbid=2634548263322091){target=_blank rel=noopener} who tested it out.
![!SKF Mud scraper](../assets/images/KYB.webp){.small}

## Kickstarter ##
This kit will add a Kickstarter in addition to the electric starter.
Please note that I haven't seen it in a SWM yet, so any feedback would be much appreciated.
More parts are needed.
[Vintage Can-Am Motorcycle Parts](https://vintagecanamparts.com/product/cam001-kickstart-hub-assembly-rotax/){target=_blank rel=noopener}

## Gas cap ##
The stock gas cap is 67 mm in diameter
![!Gas cap inner diameter](../assets/images/GasCapID.webp){.small}

Old Husqvarna P/N: 8000A8066 / 8000B1057 (need confirmation)
Husqvarna Power parts: 8000H2448 / 8000B1371
 
P/N List:
Tusk - Billet red P/N: 1086820008 (need confirmation)
Acerbis (IMS) - Carbon pattern P/N: 0016833.090
Acerbis (IMS) - Black plastic P/N: 322100-BLK
BRP - Billet black: 320002-BLK
BRP - Billet grey: 320002-GREY
Zeta - billet Black: 634-8231BK (need confirmation)
Zeta - billet red: 634-8231R (need confirmation)
Keiti: GC105R (need confirmation)
ATK: ?
Clarke: ?

[!embed](https://www.youtube.com/watch?v=YMPqokq6ecg)

??? info "Husqvarna (red) compatible"
    Husqvarna TE250 2008-2013
    Husqvarna TE 450/510 2008-2010
    Husqvarna WR/CR 125/250/300 2009-2013

??? question "Husaberg"
    Maybe the same as the following models, confirmation needed
    FE 390,450,570 P/N: 81207908000
??? question "Arctic Cat (ATV) compatible"
    Maybe the same as the following models, confirmation needed
    DVX 400 2004-2008

??? question "Kawasaki (ATV) compatible"
    Maybe the same as the following models, confirmation needed
    KFX400 2003-2006

??? question "Yamaha compatible"
    Maybe the same as the following models, confirmation needed
    XT (more information needed)
    YAMAHA XV1700 Road Star
    All Yamaha ATV (except R700):
    >
      YAMAHA YFM 350X WARRIOR (350) 87-03
      YFM 660R RAPTOR (660) 01-05
      YFM350R RAPTOR (350) 04-14
      YFS200 BLASTER (200) 88-07

??? question "Suzuki (ATV) compatible"
    Maybe the same as the following models, confirmation needed
    LTZ 250 2004-2009
    LTZ 400 2003-2014
    LTR 450 2006-2009

??? question "Honda compatible"
    Maybe the same as the following models, confirmation needed
    2004-2017 Honda CRF250R
    2004-2017 Honda CRF250X
    2002-2016 Honda CRF450R
    2017-2018 Honda CRF450RX

??? question "Aprilia compatible"
    Maybe the same as the following models, confirmation needed
    MX 50 (04)
    MX Motard 50 (95-03)
    RX 50 (03-04)
    RX Enduro 50 (95-03)
    RX racing 50 (03-06)
    MX 125 (04-06)
    RXV 4.5 450 (06)
    RXV 4.5 Pikes Peak 450 (09)
    RXV 4.5 replica 450 (06)
    SXV 4.5 450 (06)
    SXV 4.5 Pikes Peak 450 (09)
    SXV 4.5 replica 450 (06)
    RXV 5.5 550 (06)
    RXV 5.5 Pikes Peak 550 (09)
    RXV 5.5 replica 550 (06)
    SXV 5.5 550 (06)
    SXV 5.5 Pikes Peak 550 (09)
    SXV 5.5 replica 550 (06)

??? question "4MX"
    One of those should fit, need more info
    [YZ 85 / YZ 125 / YZ 250 2004-2017 | GC106RD](http://rover.ebay.com/rover/1/711-53200-19255-0/1?ff3=4&pub=5575597703&toolid=10001&campid=5338700543&customid=&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F152609742655){target=_blank rel=noopener}
    [YZF 250 / YZF 450 | GC106RD1](http://rover.ebay.com/rover/1/711-53200-19255-0/1?ff3=4&pub=5575597703&toolid=10001&campid=5338700543&customid=&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F192309838969){target=_blank rel=noopener}
    [WRF 250 / WRF 450 | GC106RD2](http://rover.ebay.com/rover/1/711-53200-19255-0/1?ff3=4&pub=5575597703&toolid=10001&campid=5338700543&customid=&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F152707942730){target=_blank rel=noopener}
    [KX 85 / KX 100 2014-2017 | GC106RD3](http://rover.ebay.com/rover/1/711-53200-19255-0/1?ff3=4&pub=5575597703&toolid=10001&campid=5338700543&customid=&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F152707946367){target=_blank rel=noopener}
    [KXF 250 / KXF 450 | GC106RD4](http://rover.ebay.com/rover/1/711-53200-19255-0/1?ff3=4&pub=5575597703&toolid=10001&campid=5338700543&customid=&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F152707947180){target=_blank rel=noopener}

[^1]: AS3 are just rebranding from some Chinese manufacturer, if you find a cheaper and non-branded alternative, _please let us know! _
[^2]: [Full thread on the US facebook group](https://www.facebook.com/groups/2147511958614522/permalink/3457154857650219/){target=_blank rel=noopener}
