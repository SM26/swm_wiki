---
title: Info and Specs
tags:
    - SM500R
---

## SM500R
![SM500R Logo](../assets/images/SM500R.webp){.big .center}

Here below is the owner's manual for the 2018-2020 SWM SM500R (Euro4) - in English only:
[Owner Manual](../assets/PDFs/EN_Full-Manual_SM-500-R_Euro-4_MY18.pdf)
<embed src="../../assets/PDFs/EN_Full-Manual_SM-500-R_Euro-4_MY18.pdf" type="application/pdf" width="100%" height="500">

Here below is the spare parts catalog for the 2018-2020 SWM SM500R (Euro4):
[Spare parts catalog](../assets/PDFs/Spare_parts_catalog-SM500R_E4_2019.pdf)
<embed src="../../assets/PDFs/Spare_parts_catalog-SM500R_E4_2019.pdf" type="application/pdf" width="100%" height="500">


### Electrical System

Here below is the electrical system schematic diagram:
![!electrical system schematic diagram SVG](../SM500R/ElectricalSchematic.svg){.center .xlarge}

#### Turn signals
The original turn indicators are of LED type, with smoked lends and a body made of rubber and plastic.
The connectors are 2 pin circuit Molex 0.062" (1.5875 mm)
![!2 Pin Molex](../assets/images/2PinMolex.webp){.xsmall}


### Oil filter
Oil drain bolt is M16 x 10mm (Total length with magnet: 20mm)
Stock oil filter P/N is: 8000B0593
HiFlo HF563 is a direct replacement
![!HF563](../assets/images/HF563.webp){.xsmall}


### Gas cap
SWM stock fuel tank cap: F000A02665
Rubber gasket part number: 8000A4562
[Other alternatives](https://sm26.gitlab.io/swm_wiki/SM500R/mods%20and%20aftermarket/#gas-cap)


### Clutch
Clutch kit (steel and friction plates) P/N: 8000A9764
Clutch O-Ring size 165x2.62 P/N: 800086878
Clutch case gasket P/N: 8000A5906

#### Clutch lever
To change the lever, you will need the following tools:
8 mm socket
4 mm hex key
[Lever options](https://sm26.gitlab.io/swm_wiki/SM500R/mods%20and%20aftermarket/#clutch-lever)

#### Clutch master
SWM P/N: F000A03382
Stock master is a Chinese copy of Magura 163 from Savage
Magura rebuilt kit **WILL NOT FIT**
  Specs:
    Mirror mounting screw hole: 10mm /1.25 pitch (clockwise)
    Oil-out screw: 10mm/ 1.0 pitch (clockwise)
    Piston diameter: 9.3 MM
    Master Cylinder diameter: 9.8 MM
    Specified use of hydraulic oil: MINERAL-OIL
    Weight: 225 g

  Braided Steel Clutch Oil Hose Pipe:
    Material: 304 stainless steel
    Screw specifications: 10mm/1.0 pitch (clockwise)
    Hose connecter diameter: 6 mm
    Hose Length: 105 mm 
    Net weight: 83 g

![!Savage 163](../assets/images/savage163.webp){.small}
[Buy link from AliExpress](http://www.aliexpress.com/item/32922178169.html){target=_blank rel=noopener}
[Alternative master](https://sm26.gitlab.io/swm_wiki/SM500R/mods%20and%20aftermarket/#clutch-master)

#### Clutch slave
SWM uses F000A00798
which is the same as the old Husqvarna 800099928
[Alternative slave](https://sm26.gitlab.io/swm_wiki/SM500R/mods%20and%20aftermarket/#clutch-slave)

### Frame Color
SWM Red color is called RAL 3020 aka "Traffic Red"
It is mostly used for the frame, so if you want to match your bling to the frame
use RAL 3020
For the new 2020 models, the new Yellow color seems to be RAL 1026 "Luminous yellow"
but any confirmation would be much appreciated.

### Spark plug
Stock P/N: M000P02418
NGK Spark plug type: CR8E(B)[^1]
NGK Iridium replacement: CR8EIX 

[^1]: B suffix mean it has a welded cap (unlike CR8E which has removable plug cap)
