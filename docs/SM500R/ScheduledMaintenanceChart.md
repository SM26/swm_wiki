---
title: Scheduled Maintenance Chart 
tags:
    - SM500R
---


# Scheduled Maintenance Chart

| SM500R | Every 500 Km | Every 2,000 Km | Every 4,000 Km | Every 8,000 Km | Every 10,000 Km | Replace if necessary | Notes |
| --- | --- | --- | --- | --- | --- | --- | --- |
|Valves|Check clearance||Check clearance||Replacement (#)||intake 0.004÷0.006 inch <br> exhaust 0.007÷0.009 inch|
|Valve Spring|||||Check|x|
|Valve Cup, Valve Half Cone|||||Check|x|
|Rocker Arm (Intake-Exhaust)|||||Check|x|
|Camshaft|||||Check||
|Valve Timing Chain||||Check|Replacement||
|Valve Timing Chain Slider|||||Replacement||
|Valve Timing Driven Gear|||||Check|x|
|Valve Timing Gear||||Check||x|
|Valve Timing Chain Tensioner|||||Check||
|Starter Decompressor System||||Check||x|
|Intake Manifold||Check||Replacement|||
|Cylinder Assy|||||Check|x|
|Piston Assy||||Check|Replacement||
|Connecting Rod Assy.|||||Replacement||
|Crankcase Bearings|||||Replacement||
|Engine Oil|Replacement|Replacement|||||
|Oil Pump||||Check|||
|Oil Filter Cartridge / Net Oil Filter|Replacement, Cleaning|Replacement, Cleaning|||||
|Oil Pump / Crankcase Hose|||||Check||
|Set Of Matched Primary|||Check||||
|Clutch Hub|||Check|||x|
|Clutch Discs||Check||Replacement||x|
|Clutch Discs Pressure Plate||||Check|||
|Clutch Spring||||Check||x|
|Clutch Discs Housing||||Check|||
|Clutch Disengagement Rod||||Check|||
|Drive Sprocket||Replacement||||x|
|Starting Gears||||Check||x|
|Gear Control Pedal||Check|||||
|Spark Plug||Cleaning|Replacement||||gap 0.027 inch|
|Air Filter|||||||
|Radiators|||||||
|Coolant Hoses And Clamps|||||||
|Radiators Hose / Water Pump|||||||
|Coolant||||||x|
|Footrests, Footrests Pins And Springs||Check||||x|
|Saddle Frame Fastening Bolts, Engine Fastening Bolts|Check||Check||||
|Side Stand|||||||
|Chain Guide Roller, Bearings|||||||
|Steering Head, Steering Crown With Pin||Greasing / Lubrication|||||
|Front Fork||Overhaul|||||
|Handlebar Holders And Fastening Set|Check||Check||||
|Rear Swing Arm Bushing|||Check||||
|Rear Chain Slider|||Check|||x|
|Rear Suspension Links Bushings|||Check||||
|Rear Chain Guide / Rear Chain Guard||||||x|
|Rear Swing Arm Pivot Needle Bearings||Greasing / Lubrication|||||
|Rear Shock Absorber|||||Overhaul||
|Rear Suspension Links Needle Bearings And Gudgeon Pin|||||||
|Throttle Control Assy|||||||
|Clutch Control Assy|||||Overhaul (#)||
|Throttle And Starting Decompressor Cables|||Greasing / Lubrication|||X|
|Front Brake Disc||Check||||x|
|Front Brake System Fluid|||||Replacement||
|Rear Brake Disc||Check||||x|
|Rear Brake System Fluid|||||Replacement||
|Brake Pads||||||x|
|Brake System Pump / Caliper Hoses|||||||
|Fuel Hoses|||||Replacement|x|
|Exhaust Silencer Packing Material||Replacement||||x|
|Exhaust Pipe And Silencer||||||x|
|Wheel Spokes Tension|Check|Check|||||
|Wheel Hub Bearings||||Replacement||x|
|Rear Driven Sprocket||Replacement||||x|
|Rear Driven Sprocket Screws Tightening|Check|Check|||||
|Rear Transmission Chain|Check, Greasing / Lubrication|Replacement||||x|
|Bolts And Nuts Tightness General Check|Check||Check||||

{!docs/abbreviations.txt!}