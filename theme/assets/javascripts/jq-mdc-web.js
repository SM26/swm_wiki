// //setup fancybox zoomable
// $(function() {
//   $('img[alt="zoomify"]').each(function(index) {
//     var imgSrc = this.src
//     var $a = $('<a href="' + imgSrc + '" data-fancybox></a>')
//     $a.fancybox()
//     $(this).before($a)
//     $a.append(this)
//   })
// });

//add target blank to newTab links
$(function() {
  $('a.new-tab').attr('target', '_blank');
});
